#!/bin/bash
function msg() {
  local text="$1"
  local div_width="80"
  printf "%${div_width}s\n" ' ' | tr ' ' -
  printf "%s\n" "$text"
}

echo "### Update And Install Deps ###"
if command -v pacman &> /dev/null
then
    sudo pacman -Syu
	sudo pacman -S --noconfirm ninja npm ranger python-pip xclip neovim base-devel lazygit ncdu ripgrep fzf base-devel opendoas lm_sensors psmisc exa
    git clone https://aur.archlinux.org/paru-bin.git
    cd paru-bin
    makepkg -si 
    paru -S brave-bin picom-jonaburg-git 
elif command -v xbps-install &> /dev/null
then
    sudo xbps-install -Syu
	sudo xbps-install -Syu xtools meson git pkg-config asciidoc MesaLib-devel dbus-devel libconfig-devel libev-devel pcre-devel pixman-devel xcb-util-image-devel xcb-util-renderutil-devel libxdg-basedir-devel uthash ninja nodejs ranger python3-pip python-pip fd xclip neovim lazygit base-devel ncdu ripgrep fzf libXft-devel libX11-devel harfbuzz-devel libXext-devel libXrender-devel libXinerama-devel base-devel psmisc opendoas lm_sensors exa
    git clone https://github.com/jonaburg/picom
    cd picom
    git submodule update --init --recursive
    meson --buildtype=release . build
    ninja -C build
    sudo ninja -C build install
    cd ../ 
    git clone https://github.com/void-linux/void-packages
    cd void-packages
    ./xbps-src binary-bootstrap
    echo XBPS_ALLOW_RESTRICTED=yes >> etc/conf
    cd srcpkgs
    git clone https://gitlab.com/ElPresidentePoole/brave-bin.git 
    cd ../
    ./xbps-src pkg brave-bin
    xbps-install --repository hostdir/binpkgs brave-bin
fi

echo "### Installing NVOID"
mkdir -p ~/.local/share/nvim/
sudo npm i -g neovim
pip install pynvim
git clone https://gitlab.com/ysfgrg/nvoid.git ~/.config/nvim
echo "### Installing Suckless programs"
git clone https://gitlab.com/ysfgrg/suckless.git ~/.config/suckless
cd ~/.config/suckless/dmenu
sudo make install
cd ../dwm
sudo make install
cd ../st
sudo make install 
cd ../slstatus
sudo make install
cd ../
echo "copying The Doas Config"
sudo cp -r doas.conf /etc/doas.conf
echo "Creating Login Manager Entry for DWM"
sudo mkdir -p /usr/share/xsessions/
cd /usr/share/xsessions
sudo touch dwm.desktop
sudo chown $USER dwm.desktop
sudo cat > dwm.desktop <<EOF
[Desktop Entry]
Encoding=UTF-8
Name=Dwm
Comment=the dynamic window manager
Exec=dwm
Icon=dwm
Type=XSession
EOF
InstallDOTS(){
echo "### Installing Dotfiles ###"
cd ~/.config/
cat > user-dirs.dirs <<EOF
# This file is written by xdg-user-dirs-update
# If you want to change or add directories, just edit the line you're
# interested in. All local changes will be retained on the next run.
# Format is XDG_xxx_DIR="$HOME/yyy", where yyy is a shell-escaped
# homedir-relative path, or XDG_xxx_DIR="/yyy", where /yyy is an
# absolute path. No other format is supported.
# 
XDG_DESKTOP_DIR="$HOME/Desktop"
XDG_DOWNLOAD_DIR="$HOME/Media/dl"
XDG_TEMPLATES_DIR="$HOME/.local/Templates"
XDG_PUBLICSHARE_DIR="$HOME/.local/Public"
XDG_DOCUMENTS_DIR="$HOME/Media/doc"
XDG_MUSIC_DIR="$HOME/Media/mus"
XDG_PICTURES_DIR="$HOME/Media/pic"
XDG_VIDEOS_DIR="$HOME/Media/vid"
EOF

mkdir ~/Media
mv ~/Downloads ~/Media/dl
mv ~/Documents ~/Media/doc
mv ~/Music ~/Media/mus
mv ~/Pictures ~/Media/pic
mv ~/Videos ~/Media/vid
mv ~/Templates ~/.local
mv ~/Public ~/.local

git clone https://gitlab.com/ysfgrg/Dotfiles ~/.dotfiles
cd -r ~/.dotfiles/
cp -r ~/.dotfiles/.xprofile ~/
cp -r ~/.dotfiles/.zshenv ~/
mkdir -p ~/.local/share/icons/
cp -r ~/.dotfiles/.local/share/icons/ePapirus-Dark/ ~/.local/share/icons/
mkdir -p ~/.local/share/themes/
cp -r ~/.dotfiles/.local/share/themes/Kripton/ ~/.local/share/themes/
cp -r ~/.dotfiles/.config/zsh/ ~/.config/
cp -r ~/.dotfiles/.config/dunst/ ~/.config/
cp -r ~/.dotfiles/.config/kitty/ ~/.config/
cp -r ~/.dotfiles/.config/picom/ ~/.config/
cp -r ~/.dotfiles/.config/ranger/ ~/.config/
cp -r ~/.dotfiles/.config/rofi/ ~/.config/
cd ~/
}
msg "Would you like to install my dotfiles?"
read -p "[y]es or [n]o (default: no) : " -r answer
[ "$answer" != "${answer#[Yy]}" ] && InstallDOTS

InstallFonts(){
echo "### Installing Fonts ###"
cd ~/post-install/
mkdir -p ~/.local/share/fonts/
cp -r ~/post-install/fonts ~/.local/share/fonts/post-install
}

msg "Would you like to install needed fonts?"
read -p "[y]es or [n]o (default: no) : " -r answer
[ "$answer" != "${answer#[Yy]}" ] && InstallFonts

echo "### {
My Tabliss config is located at ~/.dotfiles/tabliss.json
Tabliss in a perty looking Start screen for any browser base on firefox or chromium
} ###"

PS3='Set default user shell (enter number): '
shells=("bash" "zsh" "quit")
select choice in "${shells[@]}"; do
    case $choice in
          bash | zsh)
            sudo chsh $USER -s "/bin/$choice" && \
            echo -e "$choice has been set as your default USER shell. \
                    \nLogging out is required for this take effect."
            break
            ;;
         quit)
            echo "User quit without changing shell."
            break
            ;;
         *)
            echo "invalid option $REPLY"
            ;;
    esac
done

mv ~/.local/bin ~/.local/bin.bc
git clone https://gitlab.com/ysfgrg/scripts.git

msg "Would you like to reboot the system?"
read -p "[y]es or [n]o (default: no) : " -r answer
[ "$answer" != "${answer#[Yy]}" ] && doas reboot
